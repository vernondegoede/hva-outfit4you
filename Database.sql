-- -----------------------------------------------------
-- Table `gebruiker`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gebruiker` ;

CREATE  TABLE IF NOT EXISTS `gebruiker` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `datum_laatst_ingelogd` DATETIME NOT NULL ,
  `wachtwoord` VARCHAR(255) NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `voornaam` VARCHAR(255) NOT NULL ,
  `achternaam` VARCHAR(255) NOT NULL ,
  `displaynaam` VARCHAR(255) NOT NULL ,
  `adres` VARCHAR(255) NOT NULL ,
  `postcode` VARCHAR(255) NOT NULL ,
  `woonplaats` VARCHAR(255) NOT NULL ,
  `is_medewerker` INT NOT NULL DEFAULT 0 ,
  `is_manager` INT NOT NULL DEFAULT 0 ,
  `is_actief` INT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `categorie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `categorie` ;

CREATE  TABLE IF NOT EXISTS `categorie` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `parent_id` INT NOT NULL DEFAULT 0 ,
  `naam` VARCHAR(255) NOT NULL ,
  `beschrijving` TEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order` ;

CREATE  TABLE IF NOT EXISTS `order` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `klant_id` INT NOT NULL ,
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `verzend_naam` VARCHAR(255) NULL ,
  `verzend_adres` VARCHAR(255) NULL ,
  `verzend_postcode` VARCHAR(255) NULL ,
  `verzend_woonplaats` VARCHAR(255) NULL ,
  `betaalmethode` VARCHAR(255) NOT NULL DEFAULT 'pin' ,
  `opmerking` TEXT NULL ,
  `status` VARCHAR(255) NOT NULL DEFAULT 'behandeling' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product` ;

CREATE  TABLE IF NOT EXISTS `product` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `categorie_id` INT NOT NULL DEFAULT -1 ,
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `sku` VARCHAR(255) NOT NULL ,
  `naam` VARCHAR(255) NOT NULL ,
  `prijs` DOUBLE NOT NULL DEFAULT 0.00 ,
  `beschrijving` TEXT NULL ,
  `beschrijving_kort` TINYTEXT NULL ,
  `voorraad` INT NOT NULL DEFAULT -1 ,
  `afbeelding` VARCHAR(255) NULL ,
  `thumbnail` VARCHAR(255) NULL ,
  `is_actief` INT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_regel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order_regel` ;

CREATE  TABLE IF NOT EXISTS `order_regel` (
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `order_id` INT NOT NULL ,
  `product_id` INT NOT NULL ,
  `product_aantal` INT NOT NULL DEFAULT 1 )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_optie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product_optie` ;

CREATE  TABLE IF NOT EXISTS `product_optie` (
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `product_id` INT NOT NULL ,
  `naam` VARCHAR(255) NOT NULL ,
  `waarde` VARCHAR(255) NOT NULL ,
  `prijsverhoging` DOUBLE NOT NULL DEFAULT 0.00 )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `statistiek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `statistiek` ;

CREATE  TABLE IF NOT EXISTS `statistiek` (
  `id` INT NOT NULL ,
  `datum_aangemaakt` DATETIME NOT NULL ,
  `datum_gewijzigd` DATETIME NOT NULL ,
  `naam` VARCHAR(255) NOT NULL ,
  `waarde` TEXT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;
