<<<<<<< HEAD
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import main.WinkelApplication;
import model.Gebruiker;

/**
 * @version 1.0
 * @author Tjarco
 * 
 * Een klasse die het overzicht geeft van alle klanten in de database. Er kunnen vanaf hier nieuwe 
 * klanten worden toegevoegd. Ook kunenn klanten gewijzigd worden.
 */
public class KlantenOverzicht extends javax.swing.JPanel {

    /**
     * Maakt het form aan en zet de data in de tabel
     */
    public KlantenOverzicht() {
        initComponents();
        List<Gebruiker> klanten = WinkelApplication.getQueryManager().getKlantenList();
        DefaultTableModel model = (DefaultTableModel) this.jTable1.getModel();
        for (Gebruiker klant : klanten) {
            model.addRow(new Object[]{new Integer(klant.getKlantId()),
                        klant.getVoornaam(),
                        klant.getAdres(),
                        klant.getPostcode(),
                        klant.getWoonplaats()});
        }

        jtZoekveld.getDocument().addDocumentListener(new ZoekListener());
        jtZoekveld.addKeyListener(new SnelToetsListener());
    }
    
    /**
     * De listener voor sneltoetsen.
     *
     * Als de gebruiker typt om producten te zoeken kunnen de sneltoetsen omhoog
     * en naar beneden gebuikt worden om te navigeren door de rijen.
     *
     * Met de enter toets kan het geselecteerde product gewijzigd worden
     *
     */
    private class SnelToetsListener implements KeyListener {

        public void keyTyped(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
            

                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    int row = jTable1.getSelectedRow();
                    try {
                        jTable1.setRowSelectionInterval(row + 1, row + 1);
                    } catch (Exception ex) {
                        //Do nothing: last row is selected.
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    int row = jTable1.getSelectedRow();
                    try {
                        jTable1.setRowSelectionInterval(row - 1, row - 1);
                    } catch (Exception ex) {
                        //Do nothing: last row is selected.
                    }
                }

        }

        public void keyReleased(KeyEvent e) {
        }
    }

    /**
     * De listener klasse van het zoekveld. Deze klasse wordt gebruikt om naar aanpassingen in het 
     * zoekveld te luisteren, en vervolgens de rij te selecteren die met het ingetypte gedeetle overeenkomt.
     * 
     */    
   
    private class ZoekListener implements DocumentListener {

        public void insertUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }

        public void removeUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }

        public void changedUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }
        
        /**
         * Zoekt een klant op in de tabel.
         * 
         * er worden twee parameters meegegeven:
         * @param klant
         * @param field 
         * 
         * De eerste parameter is de string waarnaar gezocht moet worden in de tabel.
         * De tweede parameter is het veld waarin gezocht moet worden.
         */
        private void SearchKlant(String klant, Object field) {
            int rows = jTable1.getModel().getRowCount();
            
            int col =0;
            if(field.equals(jcVelden.getItemAt(1))) col=0;
            else if(field.equals(jcVelden.getItemAt(0))) col=1;
            else if(field.equals(jcVelden.getItemAt(2))) col=2;
            else if (field.equals(jcVelden.getItemAt(3))) col=3;
            


            for (int i = rows-1; i >=0; i--) {
                 String value = String.valueOf(jTable1.getModel().getValueAt(i, col));
   
                try {                   
                    
                    if (value.toLowerCase().contains(klant.toLowerCase()) && klant.length()!=0) {                       
                        jTable1.setRowSelectionInterval(i, i);
                    } 
                } catch (Exception e) {
                    //Do nothing
                }
            }
            
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jtZoekveld = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jcVelden = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel1.setMaximumSize(new java.awt.Dimension(1200, 400));
        jPanel1.setMinimumSize(new java.awt.Dimension(700, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(900, 400));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Klant_id", "Naam", "Adres", "Postcode"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jButton1.setText("Toevoegen");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Zoek klant:");

        jtZoekveld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtZoekveldActionPerformed(evt);
            }
        });

        jLabel2.setText("op:");

        jcVelden.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Naam", "Klant_id", "Adres", "Postcode" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtZoekveld, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcVelden, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(67, 67, 67)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 669, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(164, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtZoekveld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jcVelden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel1, new java.awt.GridBagConstraints());

        add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel3.setBackground(main.WinkelApplication.BACKGROUND);
        jPanel3.setPreferredSize(new java.awt.Dimension(961, 70));

        jLabel14.setFont(main.WinkelApplication.TITEL);
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Klantenoverzicht");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(734, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        add(jPanel3, java.awt.BorderLayout.PAGE_START);

        jPanel5.setBackground(main.WinkelApplication.BACKGROUND);

        jButton3.setFont(main.WinkelApplication.FONT_14_BOLD);
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/backButton.png"))); // NOI18N
        jButton3.setText("Terug");
        jButton3.setBorderPainted(false);
        jButton3.setContentAreaFilled(false);
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.setFocusPainted(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jButton3)
                .addContainerGap(862, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel5, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        WinkelApplication.getInstance().showPanel(new NieuweKlant(NieuweKlant.KLANTEN_OVERZICHT));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        WinkelApplication.getInstance().showPanel(new MainMenu());
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jtZoekveldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtZoekveldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtZoekveldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JComboBox jcVelden;
    private javax.swing.JTextField jtZoekveld;
    // End of variables declaration//GEN-END:variables
}
=======
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import main.WinkelApplication;
import model.Klant;

/**
 * @version 1.0
 * @author Tjarco
 * 
 * Een klasse die het overzicht geeft van alle klanten in de database. Er kunnen vanaf hier nieuwe 
 * klanten worden toegevoegd. Ook kunenn klanten gewijzigd worden.
 */
public class KlantenOverzicht extends javax.swing.JPanel {

    /**
     * Maakt het form aan en zet de data in de tabel
     */
    public KlantenOverzicht() {
        initComponents();
        List<Klant> klanten = WinkelApplication.getQueryManager().getKlantenList();
        DefaultTableModel model = (DefaultTableModel) this.jTable1.getModel();
        for (Klant klant : klanten) {
            model.addRow(new Object[]{new Integer(klant.getKlantId()),
                        klant.getNaam(),
                        klant.getAdres(),
                        klant.getPostcode(),
                        klant.getWoonplaats()});
        }

        jtZoekveld.getDocument().addDocumentListener(new ZoekListener());
        jtZoekveld.addKeyListener(new SnelToetsListener());
    }
    
    /**
     * De listener voor sneltoetsen.
     *
     * Als de gebruiker typt om producten te zoeken kunnen de sneltoetsen omhoog
     * en naar beneden gebuikt worden om te navigeren door de rijen.
     *
     * Met de enter toets kan het geselecteerde product gewijzigd worden
     *
     */
    private class SnelToetsListener implements KeyListener {

        public void keyTyped(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
            

                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    int row = jTable1.getSelectedRow();
                    try {
                        jTable1.setRowSelectionInterval(row + 1, row + 1);
                    } catch (Exception ex) {
                        //Do nothing: last row is selected.
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    int row = jTable1.getSelectedRow();
                    try {
                        jTable1.setRowSelectionInterval(row - 1, row - 1);
                    } catch (Exception ex) {
                        //Do nothing: last row is selected.
                    }
                }

        }

        public void keyReleased(KeyEvent e) {
        }
    }

    /**
     * De listener klasse van het zoekveld. Deze klasse wordt gebruikt om naar aanpassingen in het 
     * zoekveld te luisteren, en vervolgens de rij te selecteren die met het ingetypte gedeetle overeenkomt.
     * 
     */    
   
    private class ZoekListener implements DocumentListener {

        public void insertUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }

        public void removeUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }

        public void changedUpdate(DocumentEvent e) {
            SearchKlant(jtZoekveld.getText(), jcVelden.getSelectedItem());
        }
        
        /**
         * Zoekt een klant op in de tabel.
         * 
         * er worden twee parameters meegegeven:
         * @param klant
         * @param field 
         * 
         * De eerste parameter is de string waarnaar gezocht moet worden in de tabel.
         * De tweede parameter is het veld waarin gezocht moet worden.
         */
        private void SearchKlant(String klant, Object field) {
            int rows = jTable1.getModel().getRowCount();
            
            int col =0;
            if(field.equals(jcVelden.getItemAt(1))) col=0;
            else if(field.equals(jcVelden.getItemAt(0))) col=1;
            else if(field.equals(jcVelden.getItemAt(2))) col=2;
            else if (field.equals(jcVelden.getItemAt(3))) col=3;
            


            for (int i = rows-1; i >=0; i--) {
                 String value = String.valueOf(jTable1.getModel().getValueAt(i, col));
   
                try {                   
                    
                    if (value.toLowerCase().contains(klant.toLowerCase()) && klant.length()!=0) {                       
                        jTable1.setRowSelectionInterval(i, i);
                    } 
                } catch (Exception e) {
                    //Do nothing
                }
            }
            
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jtZoekveld = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jcVelden = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel1.setMaximumSize(new java.awt.Dimension(1200, 400));
        jPanel1.setMinimumSize(new java.awt.Dimension(700, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(900, 400));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(4), "jaap", null, null},
                {null, "bla", "asdf", null},
                {null, null, "gla", "1544aa"},
                { new Integer(345), null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Klant_id", "Naam", "Adres", "Postcode"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jButton1.setText("Toevoegen");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Zoek klant:");

        jtZoekveld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtZoekveldActionPerformed(evt);
            }
        });

        jLabel2.setText("op:");

        jcVelden.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Naam", "Klant_id", "Adres", "Postcode" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtZoekveld, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcVelden, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(67, 67, 67)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 669, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(164, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtZoekveld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jcVelden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel1, new java.awt.GridBagConstraints());

        add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel3.setBackground(main.WinkelApplication.BACKGROUND);
        jPanel3.setPreferredSize(new java.awt.Dimension(961, 70));

        jLabel14.setFont(main.WinkelApplication.TITEL);
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Klantenoverzicht");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(734, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        add(jPanel3, java.awt.BorderLayout.PAGE_START);

        jPanel5.setBackground(main.WinkelApplication.BACKGROUND);

        jButton3.setFont(main.WinkelApplication.FONT_14_BOLD);
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/backButton.png"))); // NOI18N
        jButton3.setText("Terug");
        jButton3.setBorderPainted(false);
        jButton3.setContentAreaFilled(false);
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.setFocusPainted(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jButton3)
                .addContainerGap(862, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel5, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        WinkelApplication.getInstance().showPanel(new NieuweKlant(NieuweKlant.KLANTEN_OVERZICHT));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        WinkelApplication.getInstance().showPanel(new MainMenu());
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jtZoekveldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtZoekveldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtZoekveldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JComboBox jcVelden;
    private javax.swing.JTextField jtZoekveld;
    // End of variables declaration//GEN-END:variables
}
>>>>>>> 7b35955ddb4ed01ed6c212a84cb854ed1f19178b
