/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * @version 1.0
 * @author Tjarco
 *
 * Klasse om een klant object te maken
 *
 * @version 2.0
 * @author Tjarco
 *
 * Klasse uitgebreid naar Gebruiker. Gebruikers kunnen klanten, medewerkers of
 * mangager zijn.
 *
 */
public class Gebruiker {

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIsActief() {
        return isActief;
    }

    public void setIsActief(boolean isActief) {
        this.isActief = isActief;
    }

    public boolean isIsManger() {
        return isManger;
    }

    public void setIsManger(boolean isManger) {
        this.isManger = isManger;
    }

    public boolean isIsMedewerker() {
        return isMedewerker;
    }

    public void setIsMedewerker(boolean isMedewerker) {
        this.isMedewerker = isMedewerker;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }
    private int Id;
    private String voornaam;
    private String achternaam;
    private String email;
    private String wachtwoord;
    private String adres;
    private String postcode;
    private String woonplaats;
    private boolean isMedewerker;
    private boolean isManger;
    private boolean isActief;

    /**
     * Constructor van een Gebruiker. De volgende parameters dienen opgegeven te worden:
     * 
     * @param Id
     * @param voornaam
     * @param achternaam
     * @param email
     * @param wachtwoord
     * @param adres
     * @param postcode
     * @param woonplaats
     * @param isMedewerker
     * @param isManger
     * @param isActief 
     */
    public Gebruiker(int Id, String voornaam, String achternaam, String email,
            String wachtwoord, String adres, String postcode, String woonplaats, boolean isMedewerker,
            boolean isManger, boolean isActief) {
        this.Id = Id;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.wachtwoord = wachtwoord;
        this.adres = adres;
        this.postcode = postcode;
        this.woonplaats = woonplaats;
        this.isMedewerker = isMedewerker;
        this.isManger = isManger;
        this.isActief = isActief;
    }
    
    public Gebruiker(){
        this.Id=0;
        this.voornaam="";
        this.achternaam="";
        this.email="";
        this.wachtwoord="";
        this.adres="";
        this.postcode="";
        this.woonplaats="";
        this.isMedewerker=false;
        this.isManger=false;
        this.isActief=false;
               
    }

    /**
     * @return the klantId
     */
    public int getKlantId() {
        return Id;
    }

    /**
     * @param klantId the klantId to set
     */
    public void setKlantId(int klantId) {
        this.Id = klantId;
    }

    /**
     * @return the naam
     */
    public String getVoornaam() {
        return voornaam;
    }

    /**
     * @param naam the naam to set
     */
    public void setVoornaam(String naam) {
        this.voornaam = naam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    /**
     * @return the adres
     */
    public String getAdres() {
        return adres;
    }

    /**
     * @param adres the adres to set
     */
    public void setAdres(String adres) {
        this.adres = adres;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the Woonplaats
     */
    public String getWoonplaats() {
        return woonplaats;
    }

    /**
     * @param Woonplaats the Woonplaats to set
     */
    public void setWoonplaats(String Woonplaats) {
        this.woonplaats = Woonplaats;
    }
}
