project-outfit
==============

Project OUTFIT

Dit is de code repository voor project OUTFIT 2012.

Om te beginnen met programmeren moeten eerst de volgende stappen uitgevoerd zijn door iedereen:

1. Iedereen moet een GitHub account hebben aangemaakt + aangeven hoe dit account heet
2. Iedereen moet een Git programma geïnstalleerd hebben (gebruik Git Bash als je al vaker met Git hebt gewerkt en GitHub als dat niet het geval is).
- URL Git Bash: http://git-scm.com/downloads
- URL van GitHub (het programma voor op je computer): http://windows.github.com/

We beginnen met programmeren van project OUTFIT vanaf week 5.